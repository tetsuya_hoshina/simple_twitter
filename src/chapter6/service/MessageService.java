package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(String userId, String start, String end) {
        final int LIMIT_NUM = 1000;
        final String START_TIME = "2020-01-01 00:00:00";
        Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
        String currentTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentTimestamp);

        Connection connection = null;
        try {
            connection = getConnection();

            Integer id = null;
            // userIdがnullまたは空文字列でない場合は、idにuserIdをセットする
            if(!StringUtils.isEmpty(userId)) {
            	id = Integer.parseInt(userId);
            }

            String startTime = null;
            // startとendの値がnullまたはから文字列であるかを判定し、絞り込み日時をセットする
            if(!StringUtils.isEmpty(start)) {
            	startTime = start + " 00:00:00";
            }else {
            	startTime = START_TIME;
            }
            
            String endTime = null;
            if(!StringUtils.isEmpty(end)) {
            	endTime = end + " 23:59:59";
            }else {
            	endTime = currentTime;
            }
            

            List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, startTime, endTime);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void delete(String messageId) {

        Connection connection = null;
        try {
            connection = getConnection();
            Integer id = Integer.parseInt(messageId);
            new MessageDao().delete(connection, id);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public Message editSelect(String messageId) {

        Connection connection = null;
        try {
            connection = getConnection();
            Integer id = Integer.parseInt(messageId);
            Message message = new MessageDao().editSelect(connection, id);
            commit(connection);

            return message;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void update(Message message) {
        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().update(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}