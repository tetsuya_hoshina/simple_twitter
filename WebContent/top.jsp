<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./css/style.css" rel="stylesheet" type="text/css">
        <script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
        <title>簡易Twitter</title>
    </head>
    <body>
        <div class="main-contents">
            <div class="header">
                <c:if test="${ empty loginUser }">
			        <a href="login">ログイン</a>
			        <a href="signup">登録する</a>
			    </c:if>
			    <c:if test="${ not empty loginUser }">
			        <a href="./">ホーム</a>
			        <a href="setting">設定</a>
			        <a href="logout">ログアウト</a>
			    </c:if>
            </div>

            <!--プロフィール表示部 -->
            <c:if test="${ not empty loginUser }">
			    <div class="profile">
			        <div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
			        <div class="account">@<c:out value="${loginUser.account}" /></div>
			        <div class="description"><c:out value="${loginUser.description}" /></div>
			    </div>
			</c:if>

			<!--絞り込み機能表示部 -->
		    <div class="narrowing-form" align="center">
		    	<form action="./" method="get">
		    		<span>日付：</span>
		    		<input type="date" id="start" name="start" value="${start}" min="2020-01-01">
		    		<span>～</span>
		    		<input type="date" id="end" name="end" value="${end}" min="2020-01-01">
		    		<input type="submit" value="絞り込み">
		    	</form>
		    </div>

			<!-- つぶやき入力時のエラーメッセージ表示部 -->
			<c:if test="${ not empty errorMessages }">
			    <div class="errorMessages">
			        <ul>
			            <c:forEach items="${errorMessages}" var="errorMessage">
			                <li><c:out value="${errorMessage}" />
			            </c:forEach>
			        </ul>
			    </div>
			    <c:remove var="errorMessages" scope="session" />
			</c:if>

			<!-- フォーム表示部 -->
			<div class="form-area">
			    <c:if test="${ isShowMessageForm }">
			        <form action="message" method="post">
			            いま、どうしてる？<br />
			            <textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
			            <br />
			            <input type="submit" value="つぶやく">（140文字まで）
			        </form>
			    </c:if>
			</div>

			<!-- つぶやき表示部 -->
			<div class="messages">
			    <c:forEach items="${messages}" var="message">
			        <div class="message">
			            <div class="account-name">
			                <span class="account">
			                	<!-- URLのリクエストパラメータにメッセージ投稿者のIDをセットする -->
			                	<a href="./?user_id=<c:out value="${message.userId}"/> ">
        							<c:out value="${message.account}" />
    							</a>
			                </span>
			                <span class="name"><c:out value="${message.name}" /></span>
			            </div>
			            <div class="text">
			            	<!-- newLineCharに改行コードをセットする -->
			            	<% pageContext.setAttribute("newLineChar", "\n"); %>
			            	<!-- テキストデータ中の改行コードで分割し、配列でデータを取得する -->
			            	<c:forEach var="messageLine" items="${fn:split(message.text, newLineChar)}">
						    	<c:out value="${messageLine}"/><br/>
						    </c:forEach>
			            </div>
			            <div class="date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
			        </div>
			        <!-- 編集ボタン -->
			        <c:if test="${ loginUser.getId() == message.userId }">
			        	<div class="edit-button">
			        		<form action="edit" method="get">
					        	<input type="hidden" name="id" value="${message.id}">
					        	<input type="submit" value="編集">
					        </form>
			        	</div>
			        </c:if>
			        <!-- 削除ボタン -->
			        <c:if test="${ loginUser.getId() == message.userId }">
				        <div class="delete-message">
					        <form action="deleteMessage" method="post">
					        	<input type="hidden" name="id" value="${message.id}">
					        	<input id="delete-message-button" type="submit" value="削除">
					        </form>
				        </div>
			        </c:if>
			        <!-- 返信フォーム -->
			        <c:if test="${ isShowMessageForm }">
				        <div class="comment-form">
					        <form action="comment" method="post">
					            返信<br />
					            <input name="id" value="${message.id}" id="id" type="hidden"/>
					            <textarea name="text" cols="100" rows="5" class="comment-box"></textarea>
					            <br />
					            <input type="submit" value="返信">（140文字まで）
					        </form>
				        </div>
			    	</c:if>
			    	<!-- 返信表示部 -->
			    	<c:forEach items="${comments}" var="comment">
			    		<c:if test="${ message.id == comment.messageId }">
					    	<div class="comment">
					            <div class="comment-account-name">
					                <span class="account"><c:out value="${comment.account}" /></span>
					                <span class="name"><c:out value="${comment.name}" /></span>
					            </div>
					            <div class="text">
					            	<!-- テキストデータ中の改行コードで分割し、配列でデータを取得する -->
					            	<c:forEach var="commentLine" items="${fn:split(comment.text, newLineChar)}">
								    	<c:out value="${commentLine}"/><br/>
								    </c:forEach>
					            </div>
					            <div class="date"><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
					        </div>
				        </c:if>
			        </c:forEach>
			    </c:forEach>
			</div>

            <div class="copyright"> Copyright(c)TetsuyaHoshina</div>
        </div>
    <script src="./javascripts/delete-message.js"></script>
    </body>
</html>